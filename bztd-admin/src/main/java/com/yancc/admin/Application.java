package com.yancc.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import org.springframework.http.HttpMethod;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseBuilder;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@EnableOpenApi
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public Docket docket(Environment environment) {
        Profiles prd = Profiles.of("prd");
        boolean flag = !environment.acceptsProfiles(prd);

        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .groupName("TM")
                .enable(flag)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.yancc.admin.controller"))
                .paths(PathSelectors.any())
                .build()
                .tags(new Tag("auth", "用户登陆/登出"),
                        new Tag("users", "用户管理"),
                        new Tag("roles", "角色管理"),
                        new Tag("menus", "菜单管理"))
                .securitySchemes(securitySchemes())
                .globalResponses(HttpMethod.GET, globalResponse())
                .globalResponses(HttpMethod.POST, globalResponse())
                .globalResponses(HttpMethod.PUT, globalResponse())
                .globalResponses(HttpMethod.DELETE, globalResponse());
    }

    private ApiInfo apiInfo() {
        Contact contact = new Contact("yancc08", "", "");

        return new ApiInfo(
                "BZTD DOCUMENT",
                "BZTD接口文档",
                "0.1",
                "",
                contact,
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList<>()
        );
    }

    private List<SecurityScheme> securitySchemes() {
        List<SecurityScheme> schemes = new ArrayList<>();
        schemes.add(new HttpAuthenticationScheme("basic", "Basic认证/Authorization: Basic base64(id:secret)", "http", "basic", "Basic", new ArrayList<>()));
        schemes.add(new HttpAuthenticationScheme("token", "Token认证/Authorization: Bearer token", "http", "bearer", "Bearer", new ArrayList<>()));
        return schemes;
    }

    private List<Response> globalResponse() {
        List<Response> responses = new ArrayList<>();
        responses.add(new ResponseBuilder().code("400").description("参数错误").build());
        responses.add(new ResponseBuilder().code("401").description("权限不足").build());
        responses.add(new ResponseBuilder().code("404").description("找不到资源").build());
        responses.add(new ResponseBuilder().code("500").description("服务器内部错误").build());
        return responses;
    }
}
