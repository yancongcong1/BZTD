package com.yancc.admin.config;

import com.p6spy.engine.common.CallableStatementInformation;
import com.p6spy.engine.common.StatementInformation;
import com.p6spy.engine.event.JdbcEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

/**
 * 实现JdbcEventListener可以自定义JDBC各个阶段访问数据库的处理<br/>
 * 注意必须使用@Component或者其它(例如@Service)对象自动扫描注解
 * github: https://github.com/gavlyukovskiy/spring-boot-data-source-decorator
 */
@Component
public class P6spyJdbcEventListener extends JdbcEventListener {

    private Logger logger = LoggerFactory.getLogger(P6spyJdbcEventListener.class);

    @Override
    public void onAfterExecuteQuery(StatementInformation statementInformation, long timeElapsedNanos, String sql, SQLException e) {
        super.onAfterExecuteQuery(statementInformation, timeElapsedNanos, sql, e);
    }

    @Override
    public void onAfterCallableStatementSet(CallableStatementInformation statementInformation, String parameterName, Object value, SQLException e) {
        super.onAfterCallableStatementSet(statementInformation, parameterName, value, e);
    }

    @Override
    public void onAfterGetResultSet(StatementInformation statementInformation, long timeElapsedNanos, SQLException e) {
        super.onAfterGetResultSet(statementInformation, timeElapsedNanos, e);
    }
}
