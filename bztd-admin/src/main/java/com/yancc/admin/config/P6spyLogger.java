package com.yancc.admin.config;

import com.p6spy.engine.logging.Category;
import com.p6spy.engine.spy.appender.FormattedLogger;
import com.p6spy.engine.spy.appender.MessageFormattingStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 自定义日志打印工具，在此可以获取sql执行的各种信息，可以自定义各种处理
 * github: https://github.com/gavlyukovskiy/spring-boot-data-source-decorator
 */
public class P6spyLogger extends FormattedLogger {

    private final static Logger logger = LoggerFactory.getLogger(P6spyLogger.class);

//    private static TimeoutNotifyService timeoutNotifyService = TimeoutNotifyService.getInstance();

    @Override
    public void logSQL(int connectionId, String now, long elapsed, Category category, String prepared, String sql, String url) {
        String message = strategy.formatMessage(connectionId, now, elapsed, category.getName(), prepared, sql, url);
        // 超时告警通知
//        if (elapsed > 2000) timeoutNotifyService.dbNotifyAdministrator(sql, elapsed);
//        logger.debug("SQL-INFO: {}", message);
    }

    @Override
    public void setStrategy(MessageFormattingStrategy strategy) {
        super.setStrategy(strategy);
    }

    @Override
    public void logException(Exception e) {

    }

    @Override
    public void logText(String text) {
    }

    @Override
    public boolean isCategoryEnabled(Category category) {
        return true;
    }
}
