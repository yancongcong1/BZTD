package com.yancc.admin.controller;

import com.yancc.admin.dto.PageReq;
import com.yancc.admin.dto.req.MenuReq;
import com.yancc.admin.dto.req.MenuUpdateReq;
import com.yancc.admin.service.MenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/v1/menus", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = "menus")
public class MenuController {

    @Autowired
    private MenuService menuService;

    @GetMapping
    @ResponseBody
    @Operation(summary = "获取全部菜单列表")
    public ResponseEntity getMenus(String description, String status) {
        return ResponseEntity.ok().body(menuService.getMenus(description, status));
    }

    @GetMapping("/page")
    @ResponseBody
    @Operation(summary = "分页获取菜单列表")
    public ResponseEntity getPageMenus(PageReq pageReq) {
        return ResponseEntity.ok().body(menuService.getPageMenus(pageReq.getSize(), pageReq.getPageNum()));
    }

    @GetMapping("/user/{id}")
    @ResponseBody
    @Operation(summary = "获取用户菜单列表")
    public ResponseEntity getUserMenus(@PathVariable @ApiParam("用户id") String id) {
        return ResponseEntity.ok().body(menuService.getUserMenus(id));
    }

    @GetMapping("/roles")
    @ResponseBody
    @Operation(summary = "获取角色菜单列表")
    public ResponseEntity getRoleMenus(@ApiParam("角色id，用(英文,)分隔") String ids) {
        return ResponseEntity.ok().body(ids == null ? null : menuService.getRoleMenus(ids));
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED, reason = "创建成功")
    @Operation(summary = "新增菜单")
    public void addMenu(@RequestBody MenuReq menuReq) {
        menuService.addMenu(menuReq);
    }

    @PutMapping
    @ResponseStatus(value = HttpStatus.RESET_CONTENT, reason = "修改成功")
    @Operation(summary = "修改菜单")
    public void updateMenu(@RequestBody MenuUpdateReq menuUpdateReq) {
        menuService.updateMenu(menuUpdateReq);
    }

}
