package com.yancc.admin.controller;

import com.yancc.admin.dto.req.RoleMenuReq;
import com.yancc.admin.dto.req.RoleReq;
import com.yancc.admin.dto.req.RoleUpdateReq;
import com.yancc.admin.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/v1/roles", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = "roles")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping
    @ResponseBody
    @Operation(summary = "获取角色列表")
    public ResponseEntity getRoles() {
        return ResponseEntity.ok().body(roleService.getRoles());
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED, reason = "创建成功")
    @Operation(summary = "新增角色")
    public void addRole(@RequestBody RoleReq roleReq) {
        roleService.addRole(roleReq);
    }

    @PutMapping
    @ResponseStatus(value = HttpStatus.RESET_CONTENT, reason = "修改成功")
    @Operation(summary = "修改角色")
    public void updateRole(@RequestBody RoleUpdateReq roleUpdateReq) {
        roleService.updateRole(roleUpdateReq);
    }

    @PostMapping(value = "/menus")
    @ResponseStatus(value = HttpStatus.RESET_CONTENT, reason = "修改成功")
    @Operation(summary = "为角色分配菜单")
    public void addRoleMenus(@RequestBody RoleMenuReq roleMenuReq) {
        roleService.addRoleMenus(roleMenuReq);
    }

}
