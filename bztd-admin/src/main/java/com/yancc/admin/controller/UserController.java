package com.yancc.admin.controller;

import com.yancc.admin.dto.req.UserConditionReq;
import com.yancc.admin.dto.req.UserReq;
import com.yancc.admin.dto.req.UserRoleReq;
import com.yancc.admin.dto.req.UserUpdateReq;
import com.yancc.admin.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/users", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = "users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED, reason = "创建成功")
    @Operation(summary = "注册新用户")
    public void registerUser(@RequestBody UserReq userReq) {
        userService.registerUser(userReq);
    }

    @GetMapping
    @ResponseBody
    @Operation(summary = "获取用户列表")
    public ResponseEntity getUsers(UserConditionReq condition) {
        return ResponseEntity.ok().body(userService.getUsers(condition));
    }

    @PutMapping
    @ResponseStatus(value = HttpStatus.RESET_CONTENT, reason = "修改成功")
    @Operation(summary = "修改用户信息")
    public void updateUser(@RequestBody UserUpdateReq userUpdateReq) {
        userService.updateUser(userUpdateReq);
    }

    @DeleteMapping
    @ResponseStatus(value = HttpStatus.RESET_CONTENT, reason = "禁用成功")
    @Operation(summary = "批量禁用用户")
    public void deleteUsers(@RequestBody @ApiParam("用户名列表") List<String> userNames) {
        userService.deleteUsers(userNames);
    }

    @PostMapping(value = "/roles")
    @ResponseStatus(value = HttpStatus.CREATED, reason = "添加角色成功")
    @Operation(summary = "为用户分配角色")
    public void addUserRoles(@RequestBody UserRoleReq userRoleReq) {
        userService.addUserRoles(userRoleReq);
    }
}
