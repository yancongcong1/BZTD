package com.yancc.admin.dto;

import lombok.Data;

@Data
public class PageReq {
    private Long size = 10L;
    private Long pageNum = 1L;
}
