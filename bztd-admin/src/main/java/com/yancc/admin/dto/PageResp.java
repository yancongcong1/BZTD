package com.yancc.admin.dto;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.Data;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Data
public class PageResp {
    // 总条数
    private Long total;
    // 每页条数
    private Long size;
    // 当前页
    private Long current;
    // 总页数
    private Long pages;
    // 结果集
    private List data;

    public static <T, R> PageResp of(IPage<T> page, Function<T, R> f) {
        PageResp response = new PageResp();
        response.setTotal(page.getTotal());
        response.setSize(page.getSize());
        response.setCurrent(page.getCurrent());
        response.setPages(page.getPages());
        response.setData(page.getRecords().stream().map(f).collect(Collectors.toList()));
        return response;
    }
}
