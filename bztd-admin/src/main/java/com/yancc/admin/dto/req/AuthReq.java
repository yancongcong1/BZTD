package com.yancc.admin.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AuthReq {
    @ApiModelProperty("用户名")
    private String username;
    @ApiModelProperty("密码")
    private String password;
}
