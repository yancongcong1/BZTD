package com.yancc.admin.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MenuReq {
    @ApiModelProperty("父菜单id")
    private Integer parentId;
    @ApiModelProperty("父菜单名称")
    private String parentName;
    @ApiModelProperty("权限标识")
    private String permission;
    @ApiModelProperty("描述")
    private String description;
    @ApiModelProperty("图标")
    private String icon;
    @ApiModelProperty("类型 D:目录 M:菜单 B:按钮")
    private Character type;
    @ApiModelProperty("在当前层级中的排序")
    private Integer orderNum;
    @ApiModelProperty("是否链接 0:是 1:否")
    private Character isLink;
    @ApiModelProperty("路由")
    private String route;
    @ApiModelProperty("是否可见 0:可见 1:不可见")
    private Character visible;
    @ApiModelProperty("状态 0:正常 1:禁用")
    private Character status;
}
