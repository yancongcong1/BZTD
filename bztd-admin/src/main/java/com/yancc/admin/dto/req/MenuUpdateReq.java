package com.yancc.admin.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MenuUpdateReq extends MenuReq {
    @ApiModelProperty("菜单id")
    private Integer id;
}
