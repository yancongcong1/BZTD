package com.yancc.admin.dto.req;

import lombok.Data;

import java.util.List;

@Data
public class RoleMenuReq {
    private Integer roleId;
    private List<Integer> menuIds;
}
