package com.yancc.admin.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class RoleReq {
    @ApiModelProperty("角色名称")
    private String name;
    @ApiModelProperty("角色描述")
    private String description;
    @ApiModelProperty("状态 0:正常 1:禁用")
    private Character status = 0;
}
