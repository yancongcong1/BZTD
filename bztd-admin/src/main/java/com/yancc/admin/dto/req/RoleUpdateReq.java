package com.yancc.admin.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class RoleUpdateReq extends RoleReq {
    @ApiModelProperty("角色id")
    private Integer id;
}
