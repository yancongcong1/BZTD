package com.yancc.admin.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UserConditionReq {
    @ApiModelProperty("用户名")
    private String username;
    @ApiModelProperty("手机号")
    private String telephone;
    @ApiModelProperty(value = "状态", allowableValues = "0,1")
    private String status;
    @ApiModelProperty("开始时间(创建)")
    private String beginTime;
    @ApiModelProperty("结束时间(创建)")
    private String endTime;
}
