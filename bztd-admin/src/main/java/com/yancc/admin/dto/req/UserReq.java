package com.yancc.admin.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UserReq {
    @ApiModelProperty("用户名")
    private String username;
    @ApiModelProperty("密码")
    private String password;
    @ApiModelProperty("电话号码")
    private String telephone;
    @ApiModelProperty(value = "状态 0:正常, 1:禁用", allowableValues = "0, 1", allowEmptyValue = true)
    private Character status = Character.UNASSIGNED;
}
