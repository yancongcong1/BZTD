package com.yancc.admin.dto.req;

import lombok.Data;

import java.util.List;

@Data
public class UserRoleReq {
    private String userId;
    private List<Integer> roleIds;
}
