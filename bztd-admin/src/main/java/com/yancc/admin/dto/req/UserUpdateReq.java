package com.yancc.admin.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UserUpdateReq {
    @ApiModelProperty("用户id")
    private String id;
    @ApiModelProperty("电话号码")
    private String telephone;
    @ApiModelProperty(value = "状态 0:正常, 1:禁用", allowableValues = "0, 1", allowEmptyValue = true)
    private Character status = Character.UNASSIGNED;
}
