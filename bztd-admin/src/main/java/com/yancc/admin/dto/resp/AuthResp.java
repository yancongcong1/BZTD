package com.yancc.admin.dto.resp;

import lombok.Data;

@Data
public class AuthResp {
    private String token;
}
