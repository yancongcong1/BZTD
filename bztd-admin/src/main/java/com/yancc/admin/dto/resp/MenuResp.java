package com.yancc.admin.dto.resp;

import lombok.Data;

import java.util.List;

@Data
public class MenuResp {
    private Integer id;
    private Integer parentId;
    private String parentName;
    private String permission;
    private String description;
    private String icon;
    private Character type;
    private Character isLink;
    private String route;
    private Character visible;
    private Character status;
    private Integer orderNum;
    private Character hasPermission = 0;
    private List<MenuResp> children;
}
