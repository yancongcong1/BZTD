package com.yancc.admin.dto.resp;

import lombok.Data;

import java.util.Date;

@Data
public class RoleResp {
    private Integer id;
    private String name;
    private String description;
    private Character status;
    private Date createTime;
}
