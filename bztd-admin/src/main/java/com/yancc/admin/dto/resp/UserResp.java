package com.yancc.admin.dto.resp;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class UserResp {
    @ApiModelProperty("用户ID")
    private String id;
    @ApiModelProperty("用户名")
    private String username;
    @ApiModelProperty("电话号码")
    private String telephone;
    @ApiModelProperty(value = "状态 0:正常, 1:禁用", allowableValues = "0, 1", allowEmptyValue = true)
    private Character status = Character.UNASSIGNED;
    @ApiModelProperty("创建时间")
    private Date createTime;
}
