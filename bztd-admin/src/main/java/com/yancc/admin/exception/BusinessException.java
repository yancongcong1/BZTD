package com.yancc.admin.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class BusinessException extends RuntimeException {
    private HttpStatus status = HttpStatus.BAD_REQUEST;

    public BusinessException() {}

    public BusinessException(HttpStatus status) {
        this.status = status;
    }

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(HttpStatus status, String message) {
        super(message);
        this.status = status;
    }
}
