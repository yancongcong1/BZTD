package com.yancc.admin.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.nio.file.AccessDeniedException;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<Map> exceptionHandler(Exception e) {
        Map<String, String> info = new HashMap<>();
        if (e instanceof MethodArgumentTypeMismatchException || e instanceof HttpMessageNotReadableException || e instanceof InvalidParameterException) {
            info.put("message", "请求参数错误");
            return new ResponseEntity<>(info, HttpStatus.BAD_REQUEST);
        } else if (e instanceof BindException) {
            FieldError fieldError = ((BindException) e).getFieldError();
            info.put("message", fieldError == null ? "请求参数错误" : fieldError.getDefaultMessage());
            return new ResponseEntity<>(info, HttpStatus.BAD_REQUEST);
        } else if (e instanceof UnauthorizedException || e instanceof AccessDeniedException) {
            info.put("message", "权限不足");
            return new ResponseEntity<>(info, HttpStatus.UNAUTHORIZED);
        } else if (e instanceof HttpRequestMethodNotSupportedException) {
            info.put("message", "请求方式错误");
            return new ResponseEntity<>(info, HttpStatus.METHOD_NOT_ALLOWED);
        } else if (e instanceof HttpClientErrorException) {
            info.put("message", ((HttpClientErrorException) e).getStatusText());
            return new ResponseEntity<>(info, ((HttpClientErrorException) e).getStatusCode());
        } else if (e instanceof BusinessException) {
            info.put("message", e.getMessage());
            log.error("[exceptionHandler error]: ", e);
            return new ResponseEntity<>(info, ((BusinessException) e).getStatus());
        } else {
            info.put("message", "服务器内部错误");
            log.error("[exceptionHandler error]: ", e);
            return new ResponseEntity<>(info, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
