package com.yancc.admin.exception;

public class UnauthorizedException extends RuntimeException {

    public UnauthorizedException() {
        super("权限不足");
    }

    public UnauthorizedException(String message) {
        super(message);
    }

}
