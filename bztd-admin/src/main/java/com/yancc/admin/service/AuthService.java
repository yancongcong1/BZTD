package com.yancc.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yancc.admin.dto.req.AuthReq;
import com.yancc.admin.dto.resp.AuthResp;
import com.yancc.admin.exception.BusinessException;
import com.yancc.repository.db.UserMapper;
import com.yancc.repository.db.entity.SysUserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
public class AuthService {

    @Autowired
    private UserService userService;
    @Autowired
    private UserMapper userMapper;

    public AuthResp login(AuthReq authReq) {
        String username = authReq.getUsername();
        QueryWrapper<SysUserEntity> wrapper = new QueryWrapper<SysUserEntity>().eq("username", username);
        SysUserEntity user = userMapper.selectOne(wrapper);

        if (!ObjectUtils.isEmpty(user)) {
            AuthResp authResp = new AuthResp();
            return authResp;
        }

        throw new BusinessException("登陆失败");
    }
}
