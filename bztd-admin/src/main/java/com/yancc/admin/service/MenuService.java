package com.yancc.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yancc.admin.dto.PageResp;
import com.yancc.admin.dto.req.MenuReq;
import com.yancc.admin.dto.resp.MenuResp;
import com.yancc.repository.db.MenuMapper;
import com.yancc.repository.db.entity.SysMenuEntity;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MenuService {

    @Autowired
    private MenuMapper menuMapper;

    /**
     * 获取菜单列表
     */
    public List<MenuResp> getMenus(String description, String status) {
        QueryWrapper<SysMenuEntity> wrapper = new QueryWrapper<SysMenuEntity>().orderByAsc("order_num", "gmt_create");
        if (StringUtils.hasText(description)) wrapper.eq("description", description);
        if (StringUtils.hasText(status)) wrapper.eq("status", status);
        List<SysMenuEntity> menuEntities = menuMapper.selectList(wrapper);
//        return menuEntities.stream().map(this::transformToMenuResp).collect(Collectors.toList());
        return transformToMenuRespListWithChildren(menuEntities);
    }

    /**
     * 分页获取菜单列表
     */
    public PageResp getPageMenus(long size, long pageNum) {
        Page<?> page = new Page<>().setSize(size).setCurrent(pageNum).addOrder(OrderItem.asc("order_num"), OrderItem.asc("gmt_create"));
        IPage<SysMenuEntity> pageInfo = menuMapper.selectPageMenus(page);
        return PageResp.of(pageInfo, this::transformToMenuResp);
    }

    /**
     * 获取用户菜单列表
     */
    public List<MenuResp> getUserMenus(String id) {
        List<SysMenuEntity> menuEntities = menuMapper.selectUserMenus(id);
        return transformToMenuRespListWithChildren(menuEntities);
    }

    /**
     * 获取角色菜单列表
     */
    public List<MenuResp> getRoleMenus(String ids) {
        List<Integer> list = Arrays.stream(ids.split(",")).map(Integer::parseInt).collect(Collectors.toList());
        List<SysMenuEntity> menuEntities = menuMapper.selectRoleMenus(list);
        return transformToMenuRespListWithChildren(menuEntities);
    }

    /**
     * 新增菜单
     */
    public void addMenu(MenuReq menuReq) {
        menuMapper.insert(transformToSysMenuEntity(menuReq));
    }

    /**
     * 新增菜单
     */
    public void updateMenu(MenuReq menuReq) {
        menuMapper.updateById(transformToSysMenuEntity(menuReq));
    }

    private List<MenuResp> transformToMenuRespListWithChildren(List<SysMenuEntity> menuEntities) {
        List<MenuResp> list = new ArrayList<>();
        menuEntities.stream().sorted(Comparator.comparingInt(SysMenuEntity::getOrderNum)).forEach(entity -> {
            if (entity.getParentId() == 0) {
                MenuResp menuResp = transformToMenuResp(entity);
                setChildren(menuResp, menuEntities);
                list.add(menuResp);
            }
        });
        return list;
    }

    private void setChildren(MenuResp selectDto, List<SysMenuEntity> menuEntities) {
        List<MenuResp> children = new ArrayList<>();
        menuEntities.stream().filter(entity -> entity.getParentId().equals(selectDto.getId())).forEach(entity -> {
            MenuResp dto = transformToMenuResp(entity);
            setChildren(dto, menuEntities);
            children.add(dto);
        });
        selectDto.setChildren(children);
    }

    private MenuResp transformToMenuResp(SysMenuEntity sysMenuEntity) {
        MenuResp selectDto = new MenuResp();
        BeanUtils.copyProperties(sysMenuEntity, selectDto);
        return selectDto;
    }

    private SysMenuEntity transformToSysMenuEntity(MenuReq menuReq) {
        SysMenuEntity entity = new SysMenuEntity();
        BeanUtils.copyProperties(menuReq, entity);
        return entity;
    }
}
