package com.yancc.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yancc.admin.dto.req.RoleMenuReq;
import com.yancc.admin.dto.req.RoleReq;
import com.yancc.admin.dto.resp.RoleResp;
import com.yancc.repository.db.RoleMapper;
import com.yancc.repository.db.RoleMenuMapper;
import com.yancc.repository.db.entity.SysRoleEntity;
import com.yancc.repository.db.entity.SysRoleMenuEntity;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoleService {

    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private RoleMenuMapper roleMenuMapper;

    /**
     * 获取角色列表
     */
    public List<RoleResp> getRoles() {
        List<SysRoleEntity> entities = roleMapper.selectList(null);
        return entities.stream().map(this::transformToRoleResp).collect(Collectors.toList());
    }

    /**
     * 新增角色
     */
    public void addRole(RoleReq roleReq) {
        roleMapper.insert(transformToSysRoleEntity(roleReq));
    }

    /**
     * 修改角色
     */
    public void updateRole(RoleReq roleReq) {
        roleMapper.updateById(transformToSysRoleEntity(roleReq));
    }

    /**
     * 为角色分配菜单
     */
    @Transactional(rollbackFor = Exception.class)
    public void addRoleMenus(RoleMenuReq roleMenuReq) {
        QueryWrapper<SysRoleMenuEntity> wrapper = new QueryWrapper<SysRoleMenuEntity>().eq("role_id", roleMenuReq.getRoleId());
        roleMenuMapper.delete(wrapper);
        roleMenuReq.getMenuIds().forEach(menuId -> {
            SysRoleMenuEntity entity = new SysRoleMenuEntity();
            entity.setRoleId(roleMenuReq.getRoleId());
            entity.setMenuId(menuId);
            roleMenuMapper.insert(entity);
        });
    }

    private RoleResp transformToRoleResp(SysRoleEntity entity) {
        RoleResp roleResp = new RoleResp();
        BeanUtils.copyProperties(entity, roleResp);
        roleResp.setCreateTime(entity.getGmtCreate());
        return roleResp;
    }

    private SysRoleEntity transformToSysRoleEntity(RoleReq roleReq) {
        SysRoleEntity entity = new SysRoleEntity();
        BeanUtils.copyProperties(roleReq, entity);
        return entity;
    }
}
