package com.yancc.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.yancc.admin.dto.req.UserConditionReq;
import com.yancc.admin.dto.req.UserReq;
import com.yancc.admin.dto.req.UserRoleReq;
import com.yancc.admin.dto.req.UserUpdateReq;
import com.yancc.admin.dto.resp.UserResp;
import com.yancc.admin.exception.BusinessException;
import com.yancc.repository.db.UserMapper;
import com.yancc.repository.db.UserRoleMapper;
import com.yancc.repository.db.entity.SysUserEntity;
import com.yancc.repository.db.entity.SysUserRoleEntity;
import com.yancc.repository.utils.KeyGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class UserService {

    private static final Pbkdf2PasswordEncoder encoder = new Pbkdf2PasswordEncoder();

    static {
        encoder.setAlgorithm(Pbkdf2PasswordEncoder.SecretKeyFactoryAlgorithm.PBKDF2WithHmacSHA512);
        encoder.setEncodeHashAsBase64(true);
    }

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserRoleMapper userRoleMapper;

    /**
     * 注册新用户
     * @param userReq
     */
    public void registerUser(UserReq userReq) {
        QueryWrapper<SysUserEntity> wrapper = new QueryWrapper<SysUserEntity>()
                .eq("username", userReq.getUsername())
                .or()
                .eq("telephone", userReq.getTelephone());
        int count = userMapper.selectCount(wrapper);
        if (count == 0) {
            userReq.setPassword(encoder.encode(userReq.getPassword()));
            SysUserEntity user = transformToSysUserEntity(userReq);
            user.setId(KeyGenerator.generateId());
            userMapper.insert(user);
        } else {
            throw new BusinessException("用户名或手机号重复");
        }
    }

    /**
     * 查询用户列表
     * @param condition
     * @return
     */
    public List<UserResp> getUsers(UserConditionReq condition) {
        QueryWrapper<SysUserEntity> wrapper = new QueryWrapper<SysUserEntity>();
        if (StringUtils.hasText(condition.getUsername())) wrapper.likeRight("username", condition.getUsername());
        if (StringUtils.hasText(condition.getTelephone())) wrapper.likeRight("telephone", condition.getTelephone());
        if (StringUtils.hasText(condition.getStatus())) wrapper.eq("status", condition.getStatus());
        if (StringUtils.hasText(condition.getBeginTime())) wrapper.ge("gmt_create", condition.getBeginTime());
        if (StringUtils.hasText(condition.getEndTime())) wrapper.le("gmt_create", condition.getEndTime());
        List<SysUserEntity> entities = userMapper.selectList(wrapper);
        return entities.stream().map(this::transformToUserResp).collect(Collectors.toList());
    }

    /**
     * 更新用户
     * @param userUpdateReq
     */
    public void updateUser(UserUpdateReq userUpdateReq) {
        userMapper.updateById(transformToSysUserEntity(userUpdateReq));
    }

    /**
     * 批量禁用用户
     * @param userNames
     */
    public void deleteUsers(List<String> userNames) {
        UpdateWrapper<SysUserEntity> wrapper = new UpdateWrapper<SysUserEntity>()
                .in("username", userNames)
                .eq("status", "0")
                .set("status", "1");
        userMapper.update(null, wrapper);
    }

    /**
     * 为用户分配角色
     * @param userRoleReq
     */
    @Transactional(rollbackFor = Exception.class)
    public void addUserRoles(UserRoleReq userRoleReq) {
        QueryWrapper<SysUserRoleEntity> wrapper = new QueryWrapper<SysUserRoleEntity>().eq("user_id", userRoleReq.getUserId());
        userRoleMapper.delete(wrapper);
        userRoleReq.getRoleIds().forEach(roleId -> {
            SysUserRoleEntity entity = new SysUserRoleEntity();
            entity.setUserId(userRoleReq.getUserId());
            entity.setRoleId(roleId);
            userRoleMapper.insert(entity);
        });
    }

    private SysUserEntity transformToSysUserEntity(UserReq userReq) {
        SysUserEntity entity = new SysUserEntity();
        entity.setUsername(userReq.getUsername());
        entity.setPassword(userReq.getPassword());
        entity.setTelephone(userReq.getTelephone());
        entity.setStatus(userReq.getStatus());
        return entity;
    }

    private SysUserEntity transformToSysUserEntity(UserUpdateReq userUpdateReq) {
        SysUserEntity entity = new SysUserEntity();
        entity.setId(userUpdateReq.getId());
        entity.setTelephone(userUpdateReq.getTelephone());
        entity.setStatus(userUpdateReq.getStatus());
        return entity;
    }

    private UserResp transformToUserResp(SysUserEntity entity) {
        UserResp userResp = new UserResp();
        userResp.setId(entity.getId());
        userResp.setUsername(entity.getUsername());
        userResp.setTelephone(entity.getTelephone());
        userResp.setStatus(entity.getStatus());
        userResp.setCreateTime(entity.getGmtCreate());
        return userResp;
    }

}
