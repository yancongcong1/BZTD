package com.yancc.common.annotation;

import java.lang.annotation.*;

/**
 * 系统日志记录
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface BcLogger {
    LoggerType type() default LoggerType.DEFAULT;
}

enum LoggerType {
    DEFAULT("default", "默认日志"),
    AUTH("auth", "操作日志"),
    OPERATION("operation", "操作日志");

    LoggerType(String type, String desc) {}
}
