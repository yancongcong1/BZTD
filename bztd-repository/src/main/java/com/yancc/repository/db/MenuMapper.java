package com.yancc.repository.db;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yancc.repository.db.entity.SysMenuEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MenuMapper extends BaseMapper<SysMenuEntity> {
    List<SysMenuEntity> selectUserMenus(@Param("id") String id);

    List<SysMenuEntity> selectRoleMenus(@Param("ids") List<Integer> ids);

    IPage<SysMenuEntity> selectPageMenus(Page<?> page);
}
