package com.yancc.repository.db;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yancc.repository.db.entity.SysRoleEntity;

public interface RoleMapper extends BaseMapper<SysRoleEntity> {
}
