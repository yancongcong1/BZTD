package com.yancc.repository.db;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yancc.repository.db.entity.SysRoleMenuEntity;

import java.util.List;

public interface RoleMenuMapper extends BaseMapper<SysRoleMenuEntity> {
    /**
     * 删除角色菜单
     * @param roleId
     * @param menuIds
     * @return
     */
    int deleteRoleMenu(Integer roleId, List<Integer> menuIds);
}
