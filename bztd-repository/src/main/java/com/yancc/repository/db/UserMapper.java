package com.yancc.repository.db;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yancc.repository.db.entity.SysUserEntity;

public interface UserMapper extends BaseMapper<SysUserEntity> {
}
