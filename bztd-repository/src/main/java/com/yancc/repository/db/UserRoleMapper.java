package com.yancc.repository.db;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yancc.repository.db.entity.SysUserRoleEntity;

public interface UserRoleMapper extends BaseMapper<SysUserRoleEntity> {
}
