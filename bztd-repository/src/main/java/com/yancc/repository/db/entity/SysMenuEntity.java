package com.yancc.repository.db.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.persistence.Column;
import java.sql.Timestamp;

@TableName(value = "sys_menu")
@Data
public class SysMenuEntity {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer parentId;
    private String parentName;
    private String permission;
    private String description;
    private String icon;
    private Character type;
    private Integer orderNum;
    private Character isLink;
    private String route;
    private Character visible;
    private Character status;
    @Column(insertable = false, updatable = false)
    private Timestamp gmtCreate;
    @Column(insertable = false, updatable = false)
    private Timestamp gmtModified;
    @TableField(exist = false)
    private Character hasPermission;
}
