package com.yancc.repository.db.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.persistence.Column;
import java.sql.Timestamp;

@TableName(value = "sys_role")
@Data
public class SysRoleEntity {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String name;
    private String description;
    private Character status;
    @Column(insertable = false, updatable = false)
    private Timestamp gmtCreate;
    @Column(insertable = false, updatable = false)
    private Timestamp gmtModified;
}
