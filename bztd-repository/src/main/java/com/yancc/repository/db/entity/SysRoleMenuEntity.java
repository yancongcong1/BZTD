package com.yancc.repository.db.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.persistence.Column;
import java.sql.Timestamp;

@TableName(value = "sys_role_menu")
@Data
public class SysRoleMenuEntity {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer roleId;
    private Integer menuId;
    @Column(insertable = false, updatable = false)
    private Timestamp gmtCreate;
    @Column(insertable = false, updatable = false)
    private Timestamp gmtModified;
}
