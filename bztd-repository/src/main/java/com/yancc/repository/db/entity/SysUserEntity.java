package com.yancc.repository.db.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.persistence.Column;
import java.sql.Timestamp;

@TableName(value = "sys_user")
@Data
public class SysUserEntity {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    private String username;
    private String password;
    private String telephone;
    private Character status;
    @Column(insertable = false, updatable = false)
    private Timestamp gmtCreate;
    @Column(insertable = false, updatable = false)
    private Timestamp gmtModified;
}
