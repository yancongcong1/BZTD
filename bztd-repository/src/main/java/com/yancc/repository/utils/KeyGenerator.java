package com.yancc.repository.utils;

import org.springframework.util.JdkIdGenerator;

/**
 * 数据库主键生成工具
 */
public class KeyGenerator {
    private static final JdkIdGenerator idGenerator = new JdkIdGenerator();

    public static String generateId() {
        return idGenerator.generateId().toString();
    }
}
