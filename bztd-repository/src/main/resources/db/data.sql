/*
 Navicat Premium Data Transfer

 Source Server         : docker-yancc
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : 10.154.21.95:3306
 Source Schema         : bztd

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 28/04/2021 17:11:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `parent_id` int(11) NOT NULL DEFAULT 0 COMMENT '父菜单id',
  `parent_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '父菜单名称',
  `permission` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '权限标识',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '描述',
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '菜单图标',
  `type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT 'M' COMMENT '菜单类型 D：目录 M：菜单 B：按钮',
  `order_num` int(11) NULL DEFAULT 0 COMMENT '菜单顺序',
  `is_link` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '1' COMMENT '是否链接 0：是 1：否',
  `route` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '菜单路由',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '可见状态 0：可见 1：不可见',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '状态 0：正常 1：禁用',
  `gmt_create` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `gmt_modified` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_name`(`permission`) USING BTREE COMMENT '名称唯一'
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 0, '根目录', 'user', '用户管理', 'el-icon-user-solid', 'D', 1, '1', '', '0', '0', '2021-04-14 16:34:41', '2021-04-28 10:51:11');
INSERT INTO `sys_menu` VALUES (2, 1, '用户管理', 'user:list', '用户列表', 'el-icon-tickets', 'M', 0, '1', '/userlist', '0', '0', '2021-04-14 16:35:28', '2021-04-27 10:15:40');
INSERT INTO `sys_menu` VALUES (3, 1, '用户管理', 'user:add', '添加用户', '', 'B', 0, '1', '', '0', '0', '2021-04-14 16:36:47', '2021-04-26 14:19:01');
INSERT INTO `sys_menu` VALUES (4, 1, '用户管理', 'user:update', '修改用户', '', 'B', 0, '1', '', '0', '0', '2021-04-19 15:55:26', '2021-04-26 14:19:00');
INSERT INTO `sys_menu` VALUES (5, 1, '用户管理', 'user:delete', '禁用用户', '', 'B', 4, '1', '', '0', '0', '2021-04-19 17:34:35', '2021-04-26 14:19:11');
INSERT INTO `sys_menu` VALUES (6, 0, '根目录', 'role', '角色管理', 'el-icon-s-help', 'D', 2, '1', '', '0', '0', '2021-04-22 19:02:04', '2021-04-27 09:32:01');
INSERT INTO `sys_menu` VALUES (7, 6, '角色管理', 'role:list', '角色列表', 'el-icon-s-data', 'M', 1, '1', '/userlist', '0', '0', '2021-04-22 19:02:28', '2021-04-28 09:19:23');
INSERT INTO `sys_menu` VALUES (8, 0, '根目录', 'menu', '菜单管理', 'el-icon-menu', 'D', 3, '1', '', '0', '0', '2021-04-23 09:55:52', '2021-04-27 17:36:44');
INSERT INTO `sys_menu` VALUES (9, 8, '菜单管理', 'menu:list', '菜单列表', 'el-icon-notebook-2', 'M', 1, '1', '/menulist', '0', '0', '2021-04-23 09:56:10', '2021-04-28 09:51:08');
INSERT INTO `sys_menu` VALUES (10, 0, '根目录', 'mission', '任务管理', 'el-icon-alarm-clock', 'D', 4, '1', '', '0', '1', '2021-04-26 16:27:49', '2021-04-27 17:37:05');
INSERT INTO `sys_menu` VALUES (11, 6, '角色管理', 'role:add', '新增角色', '', 'B', 2, '1', '', '0', '0', '2021-04-26 17:38:00', '2021-04-28 09:19:31');
INSERT INTO `sys_menu` VALUES (15, 6, '角色管理', 'role:delete', '删除角色', '', 'D', 3, '1', '', '0', '0', '2021-04-26 17:47:56', '2021-04-28 09:19:37');
INSERT INTO `sys_menu` VALUES (17, 10, '任务管理', 'mission:list', '任务列表', '', 'D', 0, '1', '', '0', '0', '2021-04-26 17:54:06', '2021-04-26 17:54:06');
INSERT INTO `sys_menu` VALUES (18, 0, '根目录', 'config', '配置中心', 'el-icon-setting', 'D', 5, '1', '', '0', '1', '2021-04-27 10:48:47', '2021-04-27 10:50:57');
INSERT INTO `sys_menu` VALUES (19, 0, '根目录', 'mail', '邮件服务', 'el-icon-receiving', 'D', 6, '1', '', '0', '1', '2021-04-27 17:26:41', '2021-04-27 17:37:16');
INSERT INTO `sys_menu` VALUES (20, 0, '根目录', 'sms', '短信服务', 'el-icon-s-comment', 'D', 7, '1', '', '0', '1', '2021-04-27 19:23:43', '2021-04-27 19:23:55');
INSERT INTO `sys_menu` VALUES (21, 0, '根目录', 'log', '日志中心', 'el-icon-folder-opened', 'D', 8, '1', '', '0', '0', '2021-04-27 19:24:47', '2021-04-27 19:24:47');
INSERT INTO `sys_menu` VALUES (27, 8, '菜单管理', 'menu:add', '新增菜单', '', 'B', 2, '1', '', '0', '0', '2021-04-28 09:51:01', '2021-04-28 10:03:22');
INSERT INTO `sys_menu` VALUES (28, 8, '菜单管理', 'menu:status', '禁用/启用菜单', '', 'B', 3, '1', '', '0', '0', '2021-04-28 09:52:29', '2021-04-28 09:52:29');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '角色名称',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '角色描述',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '状态 0：正常 1：禁用',
  `gmt_create` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `gmt_modified` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_name`(`name`) USING BTREE COMMENT '角色名唯一'
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, 'systemAdmin', '系统管理员', '0', '2021-04-14 16:32:11', '2021-04-19 17:56:08');
INSERT INTO `sys_role` VALUES (2, 'admin', '管理员', '1', '2021-04-19 18:14:23', '2021-04-19 18:16:36');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `role_id` int(11) NOT NULL DEFAULT 0 COMMENT '角色id',
  `menu_id` int(11) NOT NULL DEFAULT 0 COMMENT '菜单id',
  `gmt_create` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `gmt_modified` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_role_menu`(`role_id`, `menu_id`) USING BTREE COMMENT '角色权限唯一'
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (1, 1, 1, '2021-04-14 16:39:47', '2021-04-14 16:39:47');
INSERT INTO `sys_role_menu` VALUES (2, 1, 2, '2021-04-14 16:39:52', '2021-04-14 16:39:52');
INSERT INTO `sys_role_menu` VALUES (3, 1, 3, '2021-04-14 16:39:59', '2021-04-14 16:39:59');
INSERT INTO `sys_role_menu` VALUES (4, 2, 1, '2021-04-19 18:30:28', '2021-04-19 18:30:28');
INSERT INTO `sys_role_menu` VALUES (5, 2, 2, '2021-04-19 18:30:28', '2021-04-19 18:30:28');
INSERT INTO `sys_role_menu` VALUES (6, 2, 3, '2021-04-19 18:30:28', '2021-04-19 18:30:28');
INSERT INTO `sys_role_menu` VALUES (7, 1, 8, '2021-04-23 14:43:04', '2021-04-23 14:43:04');
INSERT INTO `sys_role_menu` VALUES (8, 1, 9, '2021-04-23 14:43:09', '2021-04-23 14:43:09');
INSERT INTO `sys_role_menu` VALUES (9, 1, 27, '2021-04-28 10:02:50', '2021-04-28 10:02:50');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '主键id',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '密码',
  `salt` char(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '加密盐',
  `telephone` varchar(13) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '手机号码',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '状态 0：正常 1：禁用',
  `gmt_create` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `gmt_modified` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', '张三1', '123456', NULL, '18334706081', '0', '2021-04-13 15:24:18', '2021-04-28 15:56:50');
INSERT INTO `sys_user` VALUES ('14c68e32-d937-42ff-bc75-5ffb9014dee3', '测试', 'NYIsQA7dgbgMQt5Jn8MW0SUhw+sJoHItT/zVJDsO1QK0v3Q3SpQVsQ==', '', '17563201526', '0', '2021-04-28 17:01:28', '2021-04-28 17:01:28');
INSERT INTO `sys_user` VALUES ('2', '李四', '123456', NULL, '13743287654', '1', '2021-04-13 15:24:38', '2021-04-14 10:15:14');
INSERT INTO `sys_user` VALUES ('3', '王五', '123456', NULL, '13734107654', '0', '2021-04-13 16:44:20', '2021-04-13 16:44:20');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `user_id` int(11) NOT NULL DEFAULT 0 COMMENT '用户id',
  `role_id` int(11) NOT NULL DEFAULT 0 COMMENT '角色id',
  `gmt_create` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `gmt_modified` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_user_role`(`user_id`, `role_id`) USING BTREE COMMENT '用户角色唯一'
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1, 1, '2021-04-19 11:55:25', '2021-04-19 11:55:25');

SET FOREIGN_KEY_CHECKS = 1;
