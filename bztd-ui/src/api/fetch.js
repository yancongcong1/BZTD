import axios from 'axios';
import {ElMessage} from 'element-plus';

const instance = axios.create({
    baseURL: process.env.VUE_APP_SERVER_URL
})

instance.defaults.headers.post['Content-Type'] = 'application/json'

instance.interceptors.request.use(function (config) {
    return config;
  }, function (error) {
    ElMessage.error('请求失败');
    return Promise.reject(error);
  });

instance.interceptors.response.use(function (response) {
    return response;
  }, function (error) {
    ElMessage.error('操作失败');
    return Promise.reject(error);
  });

export default instance;