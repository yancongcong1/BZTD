import fetch from './fetch'

// 获取菜单列表
export const getMenus = ({description, status}) => {
    return fetch({
        url: `/api/v1/menus`,
        method: 'get',
        params: {description, status}
    });
}

// 获取指定用户菜单列表
export const getUserMenus = (userId) => {
    return fetch({
        url: `/api/v1/menus/user/${userId}`,
        method: 'get'
    });
}

// 获取角色的菜单权限信息
export const getRoleMenus = (roleIds) => {
    return fetch({
        url: `/api/v1/menus/roles`,
        method: 'get',
        params: {
            ids: roleIds
        }
    });
}

// 修改菜单信息
export const addMenu = (menu) => {
    return fetch({
        url: `/api/v1/menus`,
        method: 'post',
        data: menu
    });
}

// 修改菜单信息
export const updateMenu = (menu) => {
    return fetch({
        url: `/api/v1/menus`,
        method: 'put',
        data: menu
    });
}