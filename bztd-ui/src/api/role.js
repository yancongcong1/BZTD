import fetch from './fetch'

export const getRoles = () => {
    return fetch({
        url: `/api/v1/roles`,
        method: 'get'
    });
}