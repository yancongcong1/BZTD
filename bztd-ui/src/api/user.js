import fetch from './fetch'

export const getUsers = ({username}) => {
    return fetch({
        url: `/api/v1/users`,
        method: 'get',
        params: {username}
    });
}

export const addUser = (user) => {
    return fetch({
        url: `/api/v1/users`,
        method: 'post',
        data: user
    });
}

export const updateUser = (user) => {
    return fetch({
        url: `/api/v1/users`,
        method: 'put',
        data: user
    });
}