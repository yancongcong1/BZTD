import {createRouter, createWebHistory} from 'vue-router'

import Userlist from '@/components/main/user/Userlist'
import Rolelist from '@/components/main/role/Rolelist'
import Menulist from '@/components/main/menu/Menulist'

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/userlist', component: Userlist
        },
        {
            path: '/rolelist', component: Rolelist
        },
        {
            path: '/menulist', component: Menulist
        },
        {
            path: '/',
            redirect: '/userlist'
        },
        {
            path: '/:pathMatch(.*)',
            redirect: '/'
        }
    ]
})

export default router;