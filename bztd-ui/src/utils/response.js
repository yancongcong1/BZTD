import {ElMessage} from 'element-plus';

/**
 * 处理响应结果
 * @param {Object} response 响应结果
 * @param {Function} success 成功回调
 * @param {Boolean} toast 是否toast提示
 * @param {String} message toast提示信息
 */
export const handleResp = (response, success, toast = true, message = '操作成功') => {
    // console.log(response, success, toast, message);
    // do something with response
    toast && ElMessage.success({message, type: 'success'});
    success && success();
}